#include<iostream>
#include<iomanip>
#include<cmath>

using namespace std;
void F1(int );
void F2(int*);
int main()
{
	int x=55;
	cout<<"x in degeri\n"<<endl;
	cout<<"Fonksiyonlar cagrilmadan once : "<< x <<endl;
	cout<<endl;
	F1(x);
	cout<<"F1 cagrildiktan sonra   : "<< x <<endl;
	F2(&x);
	cout<<"F2 cagrildiktan sonra   : "<< x <<endl;
	
	return 0;
	
}

void F1(int n)
{
	n=66;
	cout<<"F1 fonksiyonu icinde	: "<< n <<endl;
	
}

void F2(int *n)
{
	*n=77;
	printf("F2 fonksiyonu icinde 	: %d\n", *n);
	
}


