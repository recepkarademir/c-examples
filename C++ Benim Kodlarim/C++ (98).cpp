// 100,-250,400,125,550,901,689,450,347,700 say�lar� olan diziyi fonksiyonlar kullanarak s�ralama.
#include<iostream>

using namespace std;

int BuyukBul(int []);
void KucukBul(int []);

int main()
{
	int dizi[10]={100,-250,400,125,550,901,689,450,347,700};
	
	cout<<"Buyk eleman : "<<BuyukBul(dizi)<<endl;
	KucukBul(dizi);
	
	return 0;
}

int BuyukBul(int diz[])
{
	int buyuk=diz[0];
	
	for(int i=1;i<=9;++i)
	{
		if(diz[i]>buyuk)
		{
			buyuk=diz[i];
		}
	}
	
	return buyuk;
}

void KucukBul(int dz[])
{
	int kucuk=dz[0];
	
	for(int i=1;i<=9;++i)
	{
		if(dz[i]<kucuk)
		{
			kucuk=dz[i];
		}
	}
	
	cout<<"Kucuk eleman : "<<kucuk<<endl;
}
