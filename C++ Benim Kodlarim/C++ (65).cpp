// n ve r iki sayı isteyen n in r li permütasyonlarını bulan prg. permutasyon fonk. ile yap.
// p(n,r)=(n!)/(n-1)!  şeklinde hesaplayacak.
#include<iostream>
using namespace std;
double permutasyon(int n,int r)
{
	int faktorpay=1,faktorpayda=1,a;
	a=n-r;
	while(a!=1)
	{
		faktorpayda*=a;
		--a;
	}
	while(n!=1)
	{
		faktorpay*=n;
		--n;
	}
	
	return (faktorpay/faktorpayda);
}
int main()
{
	int N,R;
	cout<<"p(n,r) islemindeki n degerini girin : ";
	cin>>N;
	cout<<"p(n,r) islemindeki r degerini girin : ";
	cin>>R;
	cout<<"________________________________________"<<endl;
	cout<<"		     	P(n,r) = "<<permutasyon(N,R)<<endl;
	return 0;
	
}
