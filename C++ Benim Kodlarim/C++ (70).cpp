/* 3)10 elemanl� bir dizi tan�mlay�n. 
Bu dizinin ba�lang�� adresini bir g�stericiye at�n.
++*ptr,*++ptr,*++ptr,--*ptr,(*++ptr)++,*ptr,--(*++ptr) i�lemlerini bu g�stericiye uygulay�n 
ve her i�lemin sonunda ekrana g�sterin.*/

#include <iostream>
using namespace std;

int main(){
	int dizi[10]={1,2,3,4,5,6,7,8,9,10},*ptr;
	ptr=dizi;
	
	cout<<"++*ptr : "<<++*ptr<<endl;
	cout<<"*++ptr : "<<*++ptr<<endl;
	cout<<"*++ptr : "<<*++ptr<<endl;
	cout<<"--*ptr : "<<--*ptr<<endl;
	cout<<"(*++ptr)++ : "<<(*++ptr)++<<endl;
	cout<<"*ptr : "<<*ptr<<endl;
	cout<<"--(*++ptr) : "<<--(*++ptr)<<endl;

	system("PAUSE");
	return 0;
}
