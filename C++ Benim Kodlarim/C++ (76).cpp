// fonksiyona parametre olarak dizi atama.
#include<iostream>
using namespace std;
int dondur(int[]);
int main()
{
	int dizi[3]={1,2,3};
	cout<<dondur(dizi)<<endl;
	return 0;
	 
}
int dondur(int dz[])
{
	int toplam=0;
	toplam+=dz[0];
	toplam+=dz[1];
	toplam+=dz[2];
	return toplam;
}
