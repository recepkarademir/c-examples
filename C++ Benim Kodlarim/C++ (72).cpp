/*Faktöriyel hesabı yapan bir program oluşturunuz.
Program içerisindeki faktöriyel fonksiyonunu parametre olarak
bir gösterici alacak ve geriye değer döndürmeden faktöriyeli 
parametre olarak gelen değişkene atayacak şekilde tasarlayınız.*/

#include<iostream>
using namespace std;
void faktoriyel(int *);
int main()
{
	int fkt;
	cout<<"Faktoriyelini bulmak istediginiz sayiyi girin : ";
	cin>>fkt;
	faktoriyel(&fkt);
	cout<<"Girilen sayinin faktoriyeli : "<<fkt<<endl;
	system("pause");
	return 0;
}
void faktoriyel(int *ptr)//göstericiye ptr ye bir adres atanıyor.
{
	int faktor=1;
	for(int a=*ptr;a>1;--a)
	{
		faktor*=a;
	}
	*ptr=faktor;
}
