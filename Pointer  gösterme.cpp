/*void gösterici*/
#include<iostream>
#include<iomanip>
#include<cmath>

using namespace std;
int main()
{
	char kar= 'a';
	int tam= 66;
	double ger= 1.2;
	void *veri;
	
	veri= &kar;
	cout<< "Veri -> Karakter: Veri "<<*(char*) veri <<" Karakter degeri gosteriyor.\n"<<endl;

	veri= &tam;
	cout<< "Veri -> Tamsayi: Simdi veri "<<*(int*)veri <<" Tamsayi degeri gosteriyor.\n"<<endl;
	
	veri= &ger;
	cout<< "Veri -> Gercel sayi: Simdi de veri "<<*(double*)veri <<" Gercel sayi degerini gosteriyor.\n"<<endl;
	
	return 0;
}
/*jenerik pointer programda veri dir*/
