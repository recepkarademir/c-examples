/*Pointerlarda aritmetik ortalama*/
#include<iostream>
#include<iomanip>
#include<cmath>

using namespace std;
double ortalama (double dizi[],int n);
int main()

{
	double a[5]={1.1, 2.2, 3.3, 4.4, 5.5};
	double AritmetikOrt;
	AritmetikOrt=ortalama (a,5);
	cout<<"Dizinin Aritmetik Ortalamasi= "<<AritmetikOrt;
	
	return 0;
	
}

double ortalama(double dizi[], int n)

{
	double *p, toplam=0.0;
	int i;
	p=dizi;				/* veya p=&dizi[0] */
	for(i=0; i<n; i++)
		toplam+=*(p+i);
	return (toplam/n);

}
